<?php

namespace Drupal\entity_type_behaviors_css_class\Plugin\EntityTypeBehavior;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_type_behaviors\Config\BehaviorConfigFactory;
use Drupal\entity_type_behaviors\EntityTypeBehaviorBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Class.
 *
 * @package Drupal\entity_type_behaviors\Plugin\EntityTypeBehavior
 *
 * @EntityTypeBehavior(
 *   id="css_class",
 *   label=@Translation("Class"),
 *   description="Behavior that set CSS classes on elements."
 * )
 */
class CssClass extends EntityTypeBehaviorBase {

  /**
   * The entity type behaviors config factory.
   *
   * @var \Drupal\entity_type_behaviors\Config\BehaviorConfigFactory
   */
  protected $configFactory;

  /**
   * CssClass constructor.
   *
   * @param array $configuration
   *   Configuration.
   * @param $plugin_id
   *   Plugin ID.
   * @param $plugin_definition
   *   Plugin definition.
   * @param \Drupal\entity_type_behaviors\Config\BehaviorConfigFactory $config_factory
   *   The entity type behaviors config factory.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $config_factory) {
    $this->configFactory = $config_factory;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type_behaviors.config.factory'));
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigForm(array $defaultValues = []): array {
    $form['classes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Classes'),
      '#description' => $this->t('The available classes. Enter one value per line, in the format class-name|label.'),
      '#rows' => 5,
      '#default_value' => $this->getConfigValue('classes') ?? '',
    ];

    $form['multiple'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow multiple classes'),
      '#description' => $this->t('Let the user select more than one class.'),
      '#default_value' => $this->getConfigValue('multiple') ?? FALSE,
    ];

    $form['default_classes'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Set behavior classes'),
      '#description' => $this->t('Add the behavior default classes "behavior" and one per enabled behavior, such "behavior--@id".', ['@id' => $this->getPluginId()]),
      '#default_value' => $this->getConfigValue('enable_default') ?? TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(): array {
    $options = [
      '' => $this->t('- None -'),
    ] + static::getAvailableClasses($this->getConfigValue('classes'));

    $element['classes'] = [
      '#type' => $this->getConfigValue('multiple') ? 'checkboxes' : 'select',
      '#title' => $this->t('Class'),
      '#options' => $options,
      '#default_value' => $this->getValueByKey('classes') ?? FALSE,
    ];

    return $element;
  }

  /**
   * Digests the string with the list of classes.
   *
   * @param string $text_list
   *   The text string with the raw classes list.
   *
   * @return array
   *   The detected classes, in the form class_name => label.
   */
  protected static function getAvailableClasses(string $text_list) {
    $values = [];

    $list = explode("\n", $text_list);
    $list = array_map('trim', $list);
    $list = array_filter($list, 'strlen');

    foreach ($list as $text) {
      // Check for an explicit key.
      $matches = [];
      if (preg_match('/(.*)\|(.*)/', $text, $matches)) {
        // Trim key and value to avoid unwanted spaces issues.
        $key = trim($matches[1]);
        $value = trim($matches[2]);
      }
      // Otherwise see if we can use the value as the key.
      elseif ($generated_key = static::generateKey($text)) {
        $key = $generated_key;
        $value = trim($text);
      }
      // Ignore invalid entry.
      else {
        continue;
      }

      $values[$key] = $value;
    }

    return $values;
  }

  /**
   * Generates a class name from a given description.
   *
   * @param $text
   *   An label or description.
   *
   * @return string
   *   The class name from the label.
   */
  protected static function generateKey($text) {
    $key = trim(strtolower(\Drupal::transliteration()->transliterate($text)));
    return preg_replace("/[^a-z0-9_\-]/",'', $key);
  }

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
    if ($classes = $this->getValueByKey('classes') ?: []) {
      if (is_string($classes)) {
        $classes = [$classes];
      }
    }

    // Add default behavior classes.
    if ($this->getConfigValue('default_classes')) {
      $classes[] = 'behavior';
      $behavior_config = $this->configFactory->getConfigDataForEntityTypeAndBundle(
        $entity->getEntityTypeId(),
        $entity->bundle()
      );

      $enabled_behaviors = $behavior_config['behaviors'] ?? [];
      foreach (array_keys($enabled_behaviors) as $enabled_behavior_id) {
        $classes[] = 'behavior--' . Html::cleanCssIdentifier($enabled_behavior_id);
      }
    }

    foreach ($classes as $class) {
      $build['#attributes']['class'][] = Html::cleanCssIdentifier($class);
    }
  }

}
