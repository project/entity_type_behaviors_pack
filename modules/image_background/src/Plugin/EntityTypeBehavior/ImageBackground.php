<?php

namespace Drupal\entity_type_behaviors_image_background\Plugin\EntityTypeBehavior;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_type_behaviors\EntityTypeBehaviorBase;
use Drupal\field\Entity\FieldConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ImageBackground.
 *
 * @package Drupal\entity_type_behaviors\Plugin\EntityTypeBehavior
 *
 * @EntityTypeBehavior(
 *  id="image_background",
 *  label=@Translation("Image Background"),
 *  description="Image background behavior for content entities."
 * )
 */
class ImageBackground extends EntityTypeBehaviorBase {

  /**
   * The field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $fieldManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, EntityFieldManagerInterface $field_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->fieldManager = $field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigForm(array $defaultValues = []): array {
    $form['enable_per_entity'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable per entity'),
      '#description' => $this->t('It gives the content editor control over where to enable the background image.'),
      '#default_value' => $this->getConfigValue('enable_per_entity') ?? TRUE,
    ];

    $source_field_options = [
      '' => $this->t('- none -'),
    ] + $this->getAvailableFields($this->configuration['entity_type'], 'image', $this->configuration['bundle']);

    $form['source_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Source field'),
      '#description' => $this->t('Image field to be used as the source for the background image.'),
      '#options' => $source_field_options,
      '#default_value' => $this->getConfigValue('source_field') ?? '',
    ];

    $form['default_image'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default image'),
      '#description' => $this->t('Path to default background image when the source field is not defined or empty.'),
      '#default_value' => $this->getConfigValue('default_image') ?? '',
    ];

    $form['repeat'] = [
      '#type' => 'select',
      '#title' => $this->t('Repeat property'),
      '#options' => [
        '' => $this->t('- do not set -'),
        'no-repeat' => $this->t('No repeat'),
        'repeat-x' => $this->t('Repeat horizontally'),
        'repeat-y' => $this->t('Repeat vertically'),
        'repeat' => $this->t('Repeat both vertically and horizontally'),
      ],
      '#description' => $this->t('Path to default background image.'),
      '#default_value' => $this->getConfigValue('repeat') ?? '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(): array {
    if (!$this->getConfigValue('enable_per_entity')) {
      return [];
    }

    $element['image_background'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use image background'),
      '#default_value' => $this->getValueByKey('image_background') ?? FALSE,
    ];

    return $element;
  }

  /**
   * Search for fields of a given type in a given entity type.
   *
   * @param string $entity_type_id
   *   The entity type ID to search in.
   * @param string $field_type
   *   A field type, @see https://www.drupal.org/node/2302735 for a list.
   * @param string|string[] $bundles
   *   (optional) Limit to selected bundle or bundles.
   * @param array $settings_match
   *   (optional) A keyed name/value array of settings the field must match.
   *
   * @return string[]
   *   An array with the names of matching fields keyed the field id.
   */
  protected function getAvailableFields($entity_type_id, $field_type, $bundles = [], array $settings_match = []) {
    $options = [];

    if (!empty($bundles)) {
      // Params adjustment.
      if (is_string($bundles)) {
        $bundles = [$bundles];
      }

      $fields_by_type = $this->fieldManager->getFieldMapByFieldType($field_type);
      // We have filter by bundle but there are no fields of the given type in
      // any bundle of the entity type.
      if (!isset($fields_by_type[$entity_type_id])) {
        return $options;
      }
    }

    // Iterate over the storage definitions of the entity fields.
    foreach ($this->fieldManager->getFieldStorageDefinitions($entity_type_id) as $field_name => $field_storage) {
      if ($field_storage->getType() != $field_type) {
        continue;
      }

      // Filter by bundle.
      foreach ($bundles as $bundle) {
        if (!isset($fields_by_type[$entity_type_id][$field_name]['bundles'][$bundle])) {
          continue 2;
        }
        // Load the field config for the first bundle.
        if (!isset($field_config)) {
          $field_config = FieldConfig::loadByName($entity_type_id, $bundle, $field_name);
        }
      }

      // Filter by settings.
      foreach ($settings_match as $key => $value) {
        if ($field_storage->getSetting($key) != $value) {
          continue 2;
        }
      }

      $options[$field_name] = isset($field_config)
        ? $field_config->label() . ' (' . $field_name . ')'
        : $field_name;

      unset($field_config);
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
    if ($this->getConfigValue('enable_per_entity')
      && !$this->getValueByKey('image_background')) {
      return;
    }

    $image_path = $this->getConfigValue('default_image');
    if (($source_field_name = $this->getConfigValue('source_field'))
      && $entity->hasField($source_field_name)) {
      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      $source_field = $entity->get($source_field_name);
      if (!$source_field->isEmpty()) {
        $image_path = $source_field->entity->url();
      }
    }

    if (empty($image_path)) {
      // Empty source field and no default image. Nothing to do.
      return;
    }

    $style = empty($build['#attributes']['style']) ? '' : $build['#attributes']['style'] . '; ';
    $style .= 'background-image: url(' . $image_path . ')';

    if ($repeat = $this->getConfigValue('repeat')) {
      $style .= '; background-repeat: ' . $repeat;
    }

    $build['#attributes']['style'] = $style;
  }
}
